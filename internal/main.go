package internal

import (
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
	"time"
)

const baseUsage = `Manage journal entries:

Usage:

    jrnl <command> [args]

The commands are:

    new     Add a new journal entry
    list    List journal entries for a given date
`

func Main(argv []string, outw io.Writer, errw io.Writer) int {
	flagMap, err := buildFlags(errw)
	if err != nil {
		fmt.Fprintf(errw, "Failed to build args: %s\n", err)
		return 1
	}

	fs, err := parseArgs(flagMap, argv)
	if err != nil {
		fmt.Fprintf(errw, "Failed to parse args: %s\n", err)
		return 1
	}

	if fs.Lookup("help").Value.(flag.Getter).Get().(bool) || fs.Lookup("h").Value.(flag.Getter).Get().(bool) {
		fs.SetOutput(outw)
		fs.Usage()
		return 0
	}

	switch fs.Name() {
	case "new":
		now := time.Now()
		editor := getEditor()
		if err := WriteJournal(fs, &now, editor, os.Stdout); err != nil {
			fmt.Fprintf(errw, "Failed to write journal: %s\n", err)
			return 1
		} else {
			return 0
		}
	case "list":
		if err := ListJournalEntries(fs, os.Stdout); err != nil {
			fmt.Fprintf(errw, "Failed to list journals: %s\n", err)
			return 1
		} else {
			return 0
		}
	case "base":
		// Base command doesn't actually do anything
		fs.Usage()
		return 1
	default: //go-cov:skip
		panic(fmt.Sprintf("Unknown command: %s\n", fs.Name()))
	}
}

func getEditor() string {
	if editor := os.Getenv("EDITOR"); editor != "" {
		return editor
	} else {
		return "vi"
	}
}

func buildBaseFlags(fs *flag.FlagSet, errw io.Writer) {
	fs.SetOutput(errw)
	fs.Usage = func() {
		fmt.Fprint(fs.Output(), baseUsage)
	}
}

func addCommonFlags(flagMap map[string]*flag.FlagSet) {
	for _, fs := range flagMap {
		fs.Bool("h", false, "Print help")
		fs.Bool("help", false, "Print help")
	}
}

func buildFlags(errw io.Writer) (map[string]*flag.FlagSet, error) {
	flagMap := map[string]*flag.FlagSet{
		"base": flag.NewFlagSet("base", flag.ExitOnError),
	}

	if newFs, err := BuildNewFlags(errw); err != nil {
		return nil, err
	} else {
		flagMap["new"] = newFs
	}
	if listFs, err := BuildListFlags(errw); err != nil { //go-cov:skip
		// uncovered, only way to fail is with no HOME
		// but that's covered when testing the previous branch
		return nil, err
	} else {
		flagMap["list"] = listFs
	}
	buildBaseFlags(flagMap["base"], errw)
	addCommonFlags(flagMap)

	return flagMap, nil
}

func parseArgs(flagMap map[string]*flag.FlagSet, args []string) (*flag.FlagSet, error) {
	cmdFlags, err := getCmdFlags(flagMap, args)
	if err != nil {
		return nil, err
	}

	if cmdFlags.Name() != "base" {
		// TODO: this is a bit of a hack
		args = args[2:]
	} else {
		args = args[1:]
	}

	if err := cmdFlags.Parse(args); err != nil { //go-cov:skip
		return nil, err
	}
	return cmdFlags, nil
}

func getCmdFlags(flagMap map[string]*flag.FlagSet, args []string) (*flag.FlagSet, error) {
	var commandName string
	if len(args) == 1 || strings.HasPrefix(args[1], "-") {
		commandName = "base"
	} else {
		commandName = args[1]
	}

	if fs, ok := flagMap[commandName]; ok {
		return fs, nil
	} else {
		return nil, fmt.Errorf("unknown command: %s", commandName)
	}
}
