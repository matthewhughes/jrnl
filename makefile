TARGET := jrnl

.DEFAULT_GOAL = $(TARGET)

GO_FILES := $(shell git ls-files -- '*.go')
GO_MODULES := $(shell go list ./...)

$(TARGET): $(GO_FILES)
	go build .

.PHONY: test
test:
	go test $(GO_MODULES)

.PHONY: build-test-debug
build-test-debug:
	go test -gcflags='-N -l' -c gitlab.com/matthewhughes/jrnl/internal

.PHONY: lint
lint:
	go run github.com/golangci/golangci-lint/cmd/golangci-lint run

.PHONY: check_mod
check_mod:
	./bin/check_mod

coverage.out: $(GO_FILES)
	go test -coverprofile=$@ $(GO_MODULES)

.PHONY: test-coverage
test-coverage: coverage.out
	go run gitlab.com/matthewhughes/go-cov/cmd/go-cov $^ | go tool cover -func=/dev/stdin

.PHONY: clean
clean:
	-rm --force $(TARGET)
	-rm --force coverage.out
