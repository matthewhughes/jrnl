# jrnl

[![coverage
report](https://gitlab.com/matthewhughes/jrnl/badges/main/coverage.svg)](https://gitlab.com/matthewhughes/jrnl/-/commits/main)

A program for managing journals, usage:

    Manage journal entries:
    
    Usage:
    
        jrnl <command> [args]
    
    The commands are:
    
        new     Add a new journal entry
        list    List journal entries for a given date

Commands:

    Add a new (or edit an existing) journal entry
    
    Usage:
    
        jrnl new [-dir journal-directory ] [ name ]

    Print journal entries to stdout
    
    Usage:
    
        jrnl list [ -dir journal-directory ] [ year ] [ month ] [ day ]

## Development

There are `make` rules for development flows, any changes should satisfy:

    $ make lint test check_mod jrnl

There is also `make test-coverage` to view test coverage.
