package internal

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"time"
)

// defined here for ease of testing
var nowFunc = time.Now

const ISODayFormat = "2006-01-02"

func addDirArg(fs *flag.FlagSet) error {
	if home, ok := os.LookupEnv("HOME"); ok {
		fs.String("dir", fmt.Sprintf("%s/Documents/journal", home), "The directory storing journal entries")
		return nil
	} else {
		return errors.New("HOME not set, can't determine reasonable dir")
	}
}
