package internal

import (
	"flag"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	"github.com/google/shlex"
)

const newUsage = `Add a new (or edit an existing) journal entry

Usage:

    jrnl new [-dir journal-directory ] [ name ]
`

func BuildNewFlags(errw io.Writer) (*flag.FlagSet, error) {
	fs := flag.NewFlagSet("new", flag.ExitOnError)
	fs.SetOutput(errw)
	fs.Usage = func() {
		fmt.Fprint(fs.Output(), newUsage)
	}

	if err := addDirArg(fs); err != nil {
		return nil, err
	}
	return fs, nil
}

func WriteJournal(newFs *flag.FlagSet, now *time.Time, editor string, w io.Writer) error {
	journalDir := newFs.Lookup("dir").Value.(flag.Getter).Get().(string) //nolint:errcheck

	if newFs.NArg() > 1 {
		return fmt.Errorf("takes exactly one `name`\n%s", newUsage)
	}
	label := newFs.Arg(0)
	filename := getJournalFilename(now, label)

	filepath, created, err := getJournalFilepath(journalDir, filename, w)
	if err != nil {
		return err
	} else if created {
		if err = writeJournalHeader(filepath, label, now); err != nil {
			return err
		}
	}

	if err := openInEditor(editor, filepath); err != nil {
		// don't remove an existing entry
		if created {
			os.Remove(filepath) //nolint:errcheck
		}
		return fmt.Errorf("Failed to write journal %s with %s: %s", filepath, editor, err)
	}
	return nil
}

func getJournalFilename(t *time.Time, label string) string {
	nowStr := t.Format(ISODayFormat)
	if label != "" {
		return fmt.Sprintf("%s-%s.md", nowStr, label)
	} else {
		return fmt.Sprintf("%s.md", nowStr)
	}
}

func getJournalFilepath(journalDir, filename string, w io.Writer) (string, bool, error) {
	// Check if journalDir exists, if not create
	fi, err := os.Stat(journalDir)
	if err != nil {
		if os.IsNotExist(err) {
			fmt.Fprintf(w, "%s doesn't exist, creating it", journalDir)
			if err := os.MkdirAll(journalDir, 0o755); err != nil {
				return "", false, fmt.Errorf("failed creating %s: %s", journalDir, err)
			}
		} else {
			return "", false, err
		}
	} else if !fi.IsDir() {
		return "", false, fmt.Errorf("%s is not a directory", journalDir)
	}

	dest := filepath.Join(journalDir, filename)

	if _, err = os.Stat(dest); err == nil {
		return dest, false, nil
	} else if os.IsNotExist(err) {
		return dest, true, nil
	} else {
		return "", false, err
	}
}

func writeJournalHeader(journalFile, label string, t *time.Time) error {
	var content string

	if label == "" {
		content = fmt.Sprintf("# %s\n\n", t.Format(ISODayFormat))
	} else {
		content = fmt.Sprintf("# %s\n\n", label)
	}

	if err := os.WriteFile(journalFile, []byte(content), 0o600); err != nil {
		return fmt.Errorf("failed to write content to %s: %s", journalFile, err)
	}
	return nil
}

func openInEditor(editor, filepath string) error {
	editorCmd, err := shlex.Split(editor)
	if err != nil { //go-cov:skip
		panic(fmt.Sprintf("Failed to shlex: %s: %s", editor, err))
	}

	editorCmd = append(editorCmd, "--", filepath)
	cmd := exec.Command(editorCmd[0], editorCmd[1:]...) // #nosec G204 -- we // give the user explicit control over the command
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin

	return cmd.Run()
}
