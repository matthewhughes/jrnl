// Package internal contains separate files for each subcommand
// this file serves 'list'
package internal

import (
	"flag"
	"fmt"
	"io"
	"path/filepath"
)

const listUsage = `Print journal entries to stdout

Usage:

    jrnl list [ -dir journal-directory ] [ year ] [ month ] [ day ]
`

func ListJournalEntries(listFs *flag.FlagSet, out io.Writer) error {
	journalDir := listFs.Lookup("dir").Value.(flag.Getter).Get().(string) //nolint:errcheck

	narg := listFs.NArg()

	var searchStr string
	if narg == 0 {
		searchStr = nowFunc().Format(ISODayFormat)
	} else {
		switch narg {
		case 1:
			searchStr = fmt.Sprintf("%s-", listFs.Arg(0))
		case 2:
			searchStr = fmt.Sprintf("%s-%s-", listFs.Arg(0), listFs.Arg(1))
		case 3:
			searchStr = fmt.Sprintf("%s-%s-%s-", listFs.Arg(0), listFs.Arg(1), listFs.Arg(2))
		default:
			return fmt.Errorf("takes at most three arguments\n%s", listUsage)
		}
	}

	if entries, err := filepath.Glob(filepath.Join(journalDir, searchStr) + "*"); err != nil {
		return fmt.Errorf("failed reading entries under %s: %s", journalDir, err)
	} else {
		for _, entry := range entries {
			fmt.Fprintln(out, entry)
		}
		return nil
	}
}

func BuildListFlags(errw io.Writer) (*flag.FlagSet, error) {
	fs := flag.NewFlagSet("list", flag.ExitOnError)
	fs.SetOutput(errw)
	fs.Usage = func() {
		fmt.Fprint(fs.Output(), listUsage)
	}
	if err := addDirArg(fs); err != nil {
		return nil, err
	} else {
		return fs, nil
	}
}
