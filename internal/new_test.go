package internal

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"gotest.tools/v3/assert"
)

// Convenience placeholders
var testNow time.Time = time.Now()

// no-op editor
const testEditor = "true"

func IsRoot() bool {
	return os.Geteuid() == 0
}

func TestBuildNewFlagsNoHomedirError(t *testing.T) {
	expectedError := "HOME not set, can't determine reasonable dir"

	origHome := os.Getenv("HOME")
	defer os.Setenv("HOME", origHome)
	os.Unsetenv("HOME")

	_, err := BuildNewFlags(io.Discard)
	assert.Error(t, err, expectedError)
}

func parseNewArgs(t *testing.T, args []string) *flag.FlagSet {
	if _, ok := os.LookupEnv("HOME"); !ok {
		t.Skip("Skipping: test requires 'HOME' to be set")
	}
	fs, err := BuildNewFlags(io.Discard)

	assert.NilError(t, err)
	assert.NilError(t, fs.Parse(args))

	return fs
}

func TestWriteJournalTooManyArgs(t *testing.T) {
	args := []string{"something", "another-thing"}
	expectedError := "takes exactly one `name`\n" + newUsage

	fs := parseNewArgs(t, args)

	err := WriteJournal(fs, &testNow, testEditor, io.Discard)
	assert.Error(t, err, expectedError)
}

func TestWriteJournalDirCreationFailure(t *testing.T) {
	if IsRoot() {
		t.Skip("Skipping: test requires non-root for file permissions")
	}
	parentDir := filepath.Join(t.TempDir(), "unwriteable-dir")
	destDir := filepath.Join(parentDir, "some-dir")
	expectedErrorPrefix := "failed creating " + destDir + ":"

	args := []string{"-dir", destDir}
	fs := parseNewArgs(t, args)

	assert.NilError(t, os.Mkdir(parentDir, 0o555))

	err := WriteJournal(fs, &testNow, testEditor, io.Discard)

	assert.Assert(t, err != nil)
	assert.Assert(t, strings.HasPrefix(err.Error(), expectedErrorPrefix))
}

func TestWriteJournalDirStatError(t *testing.T) {
	if IsRoot() {
		t.Skip("Skipping: test requires non-root for file permissions")
	}
	parentDir := filepath.Join(t.TempDir(), "unwriteable-dir")
	destDir := filepath.Join(parentDir, "some-dir")

	args := []string{"-dir", destDir}
	fs := parseNewArgs(t, args)

	assert.NilError(t, os.Mkdir(parentDir, 0o444))

	err := WriteJournal(fs, &testNow, testEditor, io.Discard)
	assert.Assert(t, os.IsPermission(err))
}

func TestWriteJournalNonDirGiven(t *testing.T) {
	destDirFile := filepath.Join(t.TempDir(), "file-not-dir")
	expectedErr := fmt.Sprintf("%s is not a directory", destDirFile)

	args := []string{"-dir", destDirFile}
	fs := parseNewArgs(t, args)

	_, err := os.Create(destDirFile)
	assert.NilError(t, err)

	assert.Error(t, WriteJournal(fs, &testNow, testEditor, io.Discard), expectedErr)
}

func TestWriteJournalFileAccessError(t *testing.T) {
	if IsRoot() {
		t.Skip("Skipping: test requires non-root for file permissions")
	}
	destDir := filepath.Join(t.TempDir(), "no-read-access")
	filename := testNow.Format(ISODayFormat) + ".md"

	args := []string{"-dir", destDir}
	fs := parseNewArgs(t, args)

	assert.NilError(t, os.Mkdir(destDir, 0o444))

	err := WriteJournal(fs, &testNow, testEditor, io.Discard)

	assert.Assert(t, os.IsPermission(err))

	errPath := err.(*os.PathError).Path
	assert.Assert(t, filepath.Base(errPath) == filename)
}

func TestWriteJournalFileCreateNewJournalFailure(t *testing.T) {
	if IsRoot() {
		t.Skip("Skipping: test requires non-root for file permissions")
	}
	destDir := filepath.Join(t.TempDir(), "no-write-access")
	nowStr := "2022-04-23"
	filepath := filepath.Join(destDir, nowStr) + ".md"
	expectedErrorPrefix := "failed to write content to " + filepath + ":"
	args := []string{"-dir", destDir}
	fs := parseNewArgs(t, args)

	now, err := time.Parse(ISODayFormat, nowStr)
	assert.NilError(t, err)

	assert.NilError(t, os.Mkdir(destDir, 0o555))

	err = WriteJournal(fs, &now, testEditor, io.Discard)

	assert.Assert(t, err != nil)
	assert.Assert(t, strings.HasPrefix(err.Error(), expectedErrorPrefix))
}

func TestWriteJounralWriteFailure(t *testing.T) {
	var outw bytes.Buffer
	destDir := t.TempDir()
	nowStr := "2023-03-25"
	editor := "false"
	filepath := filepath.Join(destDir, nowStr) + ".md"
	expectedError := fmt.Sprintf("Failed to write journal %s with false: exit status 1", filepath)
	expectedOut := ""
	fs := parseNewArgs(t, []string{"-dir", destDir})

	now, err := time.Parse(ISODayFormat, nowStr)
	assert.NilError(t, err)

	assert.Error(t, WriteJournal(fs, &now, editor, &outw), expectedError)
	writtenOut, err := io.ReadAll(&outw)
	assert.NilError(t, err)
	assert.Equal(t, string(writtenOut), expectedOut)

	_, err = os.Stat(filepath)
	assert.ErrorIs(t, err, os.ErrNotExist)
}

func TestWriteJounralWriteFailureExistingEntry(t *testing.T) {
	destDir := t.TempDir()
	nowStr := "2023-03-25"
	filepath := filepath.Join(destDir, nowStr) + ".md"
	fs := parseNewArgs(t, []string{"-dir", destDir})

	now, err := time.Parse(ISODayFormat, nowStr)
	assert.NilError(t, err)

	// run once to write create an entry
	assert.NilError(t, WriteJournal(fs, &now, "true", io.Discard))

	assert.Assert(t, WriteJournal(fs, &now, "false", io.Discard) != nil)
	_, err = os.Stat(filepath)
	// expect file to exist even after error
	assert.NilError(t, err)
}

func TestWriteJournalFileCreatinSuccess(t *testing.T) {
	destDir := t.TempDir()
	nowStr := "2022-04-23"
	now, err := time.Parse(ISODayFormat, nowStr)

	assert.NilError(t, err)

	testCases := []struct {
		label           string
		editor          string
		exists          bool
		expectedContent string
	}{
		{"existing-file", "true", true, ""},
		{"existing-file", "true --arg-to-test-shlex", true, ""},
		{"new-file", "true", false, "# new-file\n\n"},
		{"", "true", false, fmt.Sprintf("# %s\n\n", nowStr)},
	}

	for _, tc := range testCases {
		t.Run(tc.label, func(t *testing.T) {
			label := tc.label
			exists := tc.exists
			args := []string{"-dir", destDir, label}

			fs := parseNewArgs(t, args)

			var expectedFilename string
			if label != "" {
				expectedFilename = nowStr + "-" + label + ".md"
			} else {
				expectedFilename = nowStr + ".md"
			}

			filepath := filepath.Join(destDir, expectedFilename)

			if exists {
				_, err = os.Create(filepath)
				assert.NilError(t, err)
			}

			assert.NilError(t, WriteJournal(fs, &now, tc.editor, io.Discard))

			content, err := os.ReadFile(filepath)

			assert.NilError(t, err)
			assert.Equal(t, string(content), tc.expectedContent)
		})
	}
}
