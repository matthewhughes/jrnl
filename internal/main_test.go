package internal

import (
	"bytes"
	"io"
	"os"
	"strings"
	"testing"

	"gotest.tools/v3/assert"
)

func skipIfNoHome(t *testing.T) {
	if _, ok := os.LookupEnv("HOME"); !ok {
		t.Skip("Skipping: test requires 'HOME' to be set")
	}
}

func TestGetEditor(t *testing.T) {
	testCases := []struct {
		editor   string
		expected string
	}{
		{"/bin/vi", "/bin/vi"},
		{"", "vi"},
	}

	for _, tc := range testCases {
		t.Run(tc.editor, func(t *testing.T) {
			t.Setenv("EDITOR", tc.editor)
			assert.Equal(t, getEditor(), tc.expected)
		})
	}
}

func assertMainOutput(t *testing.T, args []string, expectedReturn int, expectedOut, expectedErr string) {
	var outw, errw bytes.Buffer

	assert.Equal(t, Main(args, &outw, &errw), expectedReturn)
	writtenOut, err := io.ReadAll(&outw)
	assert.NilError(t, err)
	writtenErr, err := io.ReadAll(&errw)
	assert.NilError(t, err)

	assert.Equal(t, string(writtenOut), expectedOut)
	assert.Equal(t, string(writtenErr), expectedErr)
}

func TestMainWithNoHomeError(t *testing.T) {
	origHome := os.Getenv("HOME")
	defer os.Setenv("HOME", origHome)
	os.Unsetenv("HOME")

	expectedError := "Failed to build args: HOME not set, can't determine reasonable dir\n"

	args := []string{"jrnl"}

	assertMainOutput(t, args, 1, "", expectedError)
}

func TestMainWithInvalidCommand(t *testing.T) {
	skipIfNoHome(t)

	commandName := "unknown_command"
	expectedError := "Failed to parse args: unknown command: " + commandName + "\n"

	args := []string{"jrnl", commandName}

	assertMainOutput(t, args, 1, "", expectedError)
}

func TestMainNewJournalError(t *testing.T) {
	skipIfNoHome(t)

	args := []string{"jrnl", "new", "some-event", "another-event"}

	expectedError := "Failed to write journal: takes exactly one `name`\n" + newUsage + "\n"

	assertMainOutput(t, args, 1, "", expectedError)
}

func TestMainNewJournalSuccess(t *testing.T) {
	skipIfNoHome(t)
	t.Setenv("EDITOR", "true")

	args := []string{"jrnl", "new", "-dir", t.TempDir()}

	assert.Equal(t, Main(args, io.Discard, io.Discard), 0)
}

func TestListJournalsError(t *testing.T) {
	skipIfNoHome(t)

	args := []string{"jrnl", "list", "2022", "04", "07", "10"}

	expectedError := "Failed to list journals: takes at most three arguments\n" + listUsage + "\n"

	assertMainOutput(t, args, 1, "", expectedError)
}

func TestMainListJournalsSuccess(t *testing.T) {
	skipIfNoHome(t)

	args := []string{"jrnl", "list", "-dir", t.TempDir()}

	assert.Equal(t, Main(args, io.Discard, io.Discard), 0)
}

func TestMainBaseCommand(t *testing.T) {
	skipIfNoHome(t)

	args := []string{"jrnl"}

	expectedOut := ""
	expectedError := baseUsage

	assertMainOutput(t, args, 1, expectedOut, expectedError)
}

func TestMainHelp(t *testing.T) {
	skipIfNoHome(t)

	testCases := []struct {
		args        []string
		expectedOut string
	}{
		{[]string{"jrnl", "-help"}, baseUsage},
		{[]string{"jrnl", "new", "-help"}, newUsage},
		{[]string{"jrnal", "list", "-help"}, listUsage},
		{[]string{"jrnl", "-h"}, baseUsage},
		{[]string{"jrnl", "new", "-h"}, newUsage},
		{[]string{"jrnal", "list", "-h"}, listUsage},
	}

	for _, tc := range testCases {
		t.Run(strings.Join(tc.args[:], " "), func(t *testing.T) {
			assertMainOutput(t, tc.args, 0, tc.expectedOut, "")
		})
	}
}
