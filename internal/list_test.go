package internal

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"testing"
	"time"

	"gotest.tools/v3/assert"
)

func TestBuildListFlagsNoHomedirError(t *testing.T) {
	expectedError := "HOME not set, can't determine reasonable dir"

	origHome := os.Getenv("HOME")
	defer os.Setenv("HOME", origHome)
	os.Unsetenv("HOME")

	_, err := BuildListFlags(io.Discard)
	assert.Error(t, err, expectedError)
}

func parseListArgs(t *testing.T, args []string) *flag.FlagSet {
	if _, ok := os.LookupEnv("HOME"); !ok {
		t.Skip("Skipping: test requires 'HOME' to be set")
	}

	fs, err := BuildListFlags(io.Discard)

	assert.NilError(t, err)
	assert.NilError(t, fs.Parse(args))

	return fs
}

func TestListJournalEntriesTooManyArgs(t *testing.T) {
	args := []string{"2022", "04", "07", "10"}
	expectedError := "takes at most three arguments\n" + listUsage

	fs := parseListArgs(t, args)

	err := ListJournalEntries(fs, io.Discard)

	assert.Error(t, err, expectedError)
}

func TestListJournalEntriesReadFailure(t *testing.T) {
	if IsRoot() {
		t.Skip("Skipping: test requires non-root for file permissions")
	}
	destDir := filepath.Join(t.TempDir(), "no-write-access")
	assert.NilError(t, os.Mkdir(destDir, 0o222))
	expectedErrorPrefix := "failed reading entries under " + destDir + ":"

	args := []string{"-dir", destDir, "bad-glob-pattern-[]"}
	fs := parseListArgs(t, args)

	err := ListJournalEntries(fs, io.Discard)

	assert.Assert(t, err != nil)
	assert.Assert(t, strings.HasPrefix(err.Error(), expectedErrorPrefix))
}

func TestListJournalEntriesMatchesOnYear(t *testing.T) {
	thisYear := "2022"
	tempDir := t.TempDir()

	thisYearJournals := []string{
		fmt.Sprintf("%s-12-01-something.md", filepath.Join(tempDir, thisYear)),
		fmt.Sprintf("%s-12-02-something-else.md", filepath.Join(tempDir, thisYear)),
	}
	otherYearJournals := []string{
		filepath.Join(tempDir, "2021-12-01-something.md"),
		filepath.Join(tempDir, "2010-12-02-something-else.md"),
	}

	testListOutput(
		t,
		tempDir,
		[]string{thisYear},
		append(thisYearJournals, otherYearJournals...),
		thisYearJournals,
	)
}

func TestListJournalEntriesMatchesOnYearAndMonth(t *testing.T) {
	thisYear := "2022"
	thisMonth := "12"
	tempDir := t.TempDir()

	thisYearAndMonthJournals := []string{
		fmt.Sprintf("%s-%s-01-something.md", filepath.Join(tempDir, thisYear), thisMonth),
		fmt.Sprintf("%s-%s-02-something-else.md", filepath.Join(tempDir, thisYear), thisMonth),
	}
	thisYearDifferentMonthJournals := []string{
		fmt.Sprintf("%s-11-01-something.md", filepath.Join(tempDir, thisYear)),
		fmt.Sprintf("%s-09-02-something-else.md", filepath.Join(tempDir, thisYear)),
	}
	differentYearJournals := []string{
		filepath.Join(tempDir, "2021-12-01-something.md"),
		filepath.Join(tempDir, "2010-09-01-something.md"),
	}

	allJournals := append(thisYearAndMonthJournals, thisYearDifferentMonthJournals...)
	allJournals = append(allJournals, differentYearJournals...)
	testListOutput(
		t,
		tempDir,
		[]string{thisYear, thisMonth},
		allJournals,
		thisYearAndMonthJournals,
	)
}

func TestListJournalEntriesMatchesOnYearAndMonthAndDay(t *testing.T) {
	thisYear := "2022"
	thisMonth := "12"
	thisDay := "28"
	tempDir := t.TempDir()

	thisYearAndMonthAndDayJournals := []string{
		fmt.Sprintf("%s-%s-%s-something.md", filepath.Join(tempDir, thisYear), thisMonth, thisDay),
		fmt.Sprintf("%s-%s-%s-something-else.md", filepath.Join(tempDir, thisYear), thisMonth, thisDay),
	}
	thisYearDifferentDayJournals := []string{
		fmt.Sprintf("%s-%s-01-something.md", filepath.Join(tempDir, thisYear), thisMonth),
		fmt.Sprintf("%s-%s-02-something-else.md", filepath.Join(tempDir, thisYear), thisMonth),
	}
	thisYearDifferentMonthJournals := []string{
		fmt.Sprintf("%s-11-01-something.md", filepath.Join(tempDir, thisYear)),
		fmt.Sprintf("%s-09-02-something-else.md", filepath.Join(tempDir, thisYear)),
	}
	differentYearJournals := []string{
		filepath.Join(tempDir, "2021-12-01-something.md"),
		filepath.Join(tempDir, "2010-09-01-something.md"),
	}
	allJournals := append(thisYearAndMonthAndDayJournals, thisYearDifferentDayJournals...)
	allJournals = append(allJournals, thisYearDifferentMonthJournals...)
	allJournals = append(allJournals, differentYearJournals...)

	testListOutput(
		t,
		tempDir,
		[]string{thisYear, thisMonth, thisDay},
		allJournals,
		thisYearAndMonthAndDayJournals,
	)
}

func TestListJournalEntriesDefaultsToJournalsFromToday(t *testing.T) {
	today := "2022-12-27"
	tempDir := t.TempDir()

	now, err := time.Parse(ISODayFormat, today)
	assert.NilError(t, err)
	oldNowFunc := nowFunc
	defer func() {
		nowFunc = oldNowFunc
	}()
	nowFunc = func() time.Time {
		return now
	}

	todayJournals := []string{
		fmt.Sprintf("%s-something.md", filepath.Join(tempDir, today)),
		fmt.Sprintf("%s-something-else.md", filepath.Join(tempDir, today)),
	}
	otherJournals := []string{
		filepath.Join(tempDir, "2022-12-21-something.md"),
		filepath.Join(tempDir, "2022-09-10-something-else.md"),
		filepath.Join(tempDir, "2021-12-28-something.md"),
		filepath.Join(tempDir, "2021-09-10-something-else.md"),
	}
	allJournals := append(todayJournals, otherJournals...)

	testListOutput(
		t,
		tempDir,
		[]string{},
		allJournals,
		todayJournals,
	)
}

func testListOutput(t *testing.T, tempDir string, extraArgs []string, allJournals []string, expected []string) {
	for _, fname := range allJournals {
		_, err := os.Create(fname)
		assert.NilError(t, err)
	}
	args := append([]string{"-dir", tempDir}, extraArgs...)
	fs := parseListArgs(t, args)

	expectedLines := append(expected, "")

	var w bytes.Buffer

	assert.NilError(t, ListJournalEntries(fs, &w))

	written, err := io.ReadAll(&w) //nolint:errcheck
	assert.NilError(t, err)
	writtenLines := strings.Split(string(written), "\n")

	// Ordering from glob is not guaranteed
	sort.Strings(writtenLines)
	sort.Strings(expectedLines)

	assert.DeepEqual(t, writtenLines, expectedLines)
}
