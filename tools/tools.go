//go:build tools

// https://github.com/golang/go/issues/25922
package tools

import (
	_ "github.com/golangci/golangci-lint/cmd/golangci-lint"
	_ "gitlab.com/matthewhughes/go-cov/cmd/go-cov"
)
