package main

import (
	"os"

	"gitlab.com/matthewhughes/jrnl/internal"
)

func main() {
	os.Exit(internal.Main(os.Args, os.Stdout, os.Stderr))
}
